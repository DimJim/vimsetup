Clojure Plugins:
================

Plugins for working with clojure.

vim-clojure-static:
-------------------

See [Github][vimcloj] for more.

Basic syntax tools: syntax highlighting, indentation, autocomplete for certain names.

[vimcloj]: https://github.com/guns/vim-clojure-static

vim-racket:
-----------

See [Github][vimrack] for more.

Basic syntax tools for Racket flavor of Lisp.

[vimrack]: https://github.com/wlangstroth/vim-racket

